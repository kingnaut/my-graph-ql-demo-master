export default {
    cities : [
        {id: 36, name: 'thanh hóa'},
        {id: 29, name: 'hà nội'},
        {id: 37, name: 'nghệ an'}
    ],
    persons: [
        { name: 'manh tuan', age: 23 , gender: 'nam'},
        { name: 'van anh', age: 21 , gender: 'nu'},
        { name: 'thien nhi', age: 22 , gender: 'nu'},
        { name: 'hung manh', age: 89 , gender: 'nam'},
        { name: 'swain', age: 27 , gender: 'nam'}
    ]

}