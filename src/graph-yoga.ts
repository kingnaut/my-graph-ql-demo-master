import { GraphQLServer } from 'graphql-yoga'
import * as db from './db'

class GraphYoga {
    public server: GraphQLServer

    private readonly typeDefs = [
        `
        type Query {
            hello(name: String): String!
            product(id: Int): City
            products: [City]
            getperson_name(name:String): [Person]
            getperson_gender(gender:String):[Person]
            GetAllPersons:[Person]
        }
        type City {
            id: Int!,
            name: String!
        }
        type Person {
            
            name: String!,
            age: Int!,
            gender: String!

        }
        `
    ]

    private readonly resolvers = {
        Query: {
            hello: (_, { name }) => `Hello ${name || 'World 1'}`,
            product: (_, { id }) => {
                return db.default.cities.find((e) => {return e.id == id})
            },
            products: () => {
                return db.default.cities
            },
            getperson_name:(_,{name})=>{
                return db.default.persons.filter((e) =>{return e.name == name} )
            },
            getperson_gender:(_,{gender})=>{
                return db.default.persons.filter((e) =>{return e.gender == gender} )
            },
           
            GetAllPersons:() => {
                return db.default.persons
            }
        }
    }
    

    constructor () {
        this.server = new GraphQLServer({
            typeDefs: this.typeDefs,
            resolvers: this.resolvers
        })
    }
}

export default new GraphYoga().server
