"use strict";
exports.__esModule = true;
var graphql_yoga_1 = require("graphql-yoga");
var db = require("./db");
var GraphYoga = /** @class */ (function () {
    function GraphYoga() {
        this.typeDefs = [
            "\n        type Query {\n            hello(name: String): String!\n            product(id: Int): City\n            products: [City]\n            getperson_name(name:String): [Person]\n            getperson_gender(gender:String): [Person]\n            GetAllPersons:[Person]\n        }\n        type City {\n            id: Int!,\n            name: String!\n        }\n        type Person {\n            \n            name: String!,\n            age: Int!,\n            gender: String!\n\n        }\n        "
        ];
        this.resolvers = {
            Query: {
                hello: function (_, _a) {
                    var name = _a.name;
                    return "Hello " + (name || 'World 1');
                },
                product: function (_, _a) {
                    var id = _a.id;
                    return db["default"].cities.find(function (e) { return e.id == id; });
                },
                products: function () {
                    return db["default"].cities;
                },
                getperson_name: function (_, _a) {
                    var name = _a.name;
                    return db["default"].persons.filter(function (e) { return e.name == name; });
                },
                getperson_gender: function (_, _a) {
                    var gender = _a.gender;
                    return db["default"].persons.filter(function (e) { return e.gender == gender; });
                },
                GetAllPersons: function () {
                    return db["default"].persons;
                }
            }
        };
        this.server = new graphql_yoga_1.GraphQLServer({
            typeDefs: this.typeDefs,
            resolvers: this.resolvers
        });
    }
    return GraphYoga;
}());
exports["default"] = new GraphYoga().server;
